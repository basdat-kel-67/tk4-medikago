from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label = 'Username', max_length = 50, required = True)
    password = forms.CharField(label = 'Password', required = True, widget=forms.PasswordInput())

class AdminForm(forms.Form):
    username = forms.CharField(label = 'Username', max_length = 50, required = True)
    password = forms.CharField(label = 'Password', required = True, widget=forms.PasswordInput())
    nomor_identitas = forms.CharField(label = 'No Identitas', max_length = 50, required = True)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length = 50, required = True)
    tanggal_lahir = forms.DateField(label = 'Tanggal Lahir', widget=forms.DateInput(attrs={
        'placeholder' : 'Day/Month/Year',
        'type' : 'date',
        'required': True,
    }))
    email = forms.EmailField(label = 'Email', max_length = 50, required = True, widget=forms.EmailInput(attrs={
		'placeholder' : 'example@example.com'
	}))
    alamat = forms.CharField(label = 'Alamat')

class DokterForm(forms.Form):
    username = forms.CharField(label = 'Username', max_length = 50, required = True)
    password = forms.CharField(label = 'Password', required = True, widget=forms.PasswordInput())
    nomor_identitas = forms.CharField(label = 'No Identitas', max_length = 50, required = True)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length = 50, required = True)
    tanggal_lahir = forms.DateField(label = 'Tanggal Lahir', widget=forms.DateInput(attrs={
        'placeholder' : 'Day/Month/Year',
        'type' : 'date',
        'required': True,
    }))
    email = forms.EmailField(label = 'Email', max_length = 50, required = True, widget=forms.EmailInput(attrs={
		'placeholder' : 'example@example.com'
	}))
    alamat = forms.CharField(label = 'Alamat')
    no_sip = forms.CharField(label = 'No SIP', max_length = 50, required = True)
    spesialisasi = forms.CharField(label = 'Spesialisasi', required = True)

class PasienForm(forms.Form):
    username = forms.CharField(label = 'Username', max_length = 50, required = True)
    password = forms.CharField(label = 'Password', required = True, widget=forms.PasswordInput())
    nomor_identitas = forms.CharField(label = 'No Identitas', max_length = 50, required = True)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length = 50, required = True)
    tanggal_lahir = forms.DateField(label = 'Tanggal Lahir', widget=forms.DateInput(attrs={
        'placeholder' : 'Day/Month/Year',
        'type' : 'date',
        'required': True,
    }))
    email = forms.EmailField(label = 'Email', max_length = 50, required = True, widget=forms.EmailInput(attrs={
		'placeholder' : 'example@example.com'
	}))
    alamat = forms.CharField(label = 'Alamat')

class AlergiForm(forms.Form):
    alergi = forms.CharField(label = 'Alergi')