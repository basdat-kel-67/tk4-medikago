[![pipeline status](https://gitlab.com/basdat-kel-67/tk4-medikago/badges/master/pipeline.svg)](https://gitlab.com/basdat-kel-67/tk4-medikago/-/commits/master) 

# Medikago
#### Kelompok 67 | Basdat E | R

### Anggota:
    1. Astrida Nayla Fauzia
    2. Nethania Sonya Violencia Lasmaria 
    3. Raul Arrafi Delfarra
    4. Muhammad Mudrik

### Tentang Project
Medika Permata Group adalah sebuah jaringan rumah sakit swasta di Indonesia. Guna meningkatkan pelayanan kesehatan, Medika Permata Group akan membuat aplikasi bernama "Medika-Go". 

Medika-Go menyediakan pelayanan pendaftaran pasien rumah sakit yang bernaung dibawah Medika Permata Group. Dengan adanya aplikasi ini, diharapkan pasien dapat memilih pelayanan rumah sakit yang sesuai dengan kebutuhan dan lokasi mereka.

### Project Link
http://medikago-kel67.herokuapp.com/