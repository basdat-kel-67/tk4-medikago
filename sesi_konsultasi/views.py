from django.shortcuts import render, redirect
from .forms import BuatSesiKonsultasi, UpdateSesiKonsultasi
from django.http import HttpResponse
from django.db import connection

def dictfetchall(cursor):
	columns = [col[0] for col in cursor.description]
	return [
		dict(zip(columns, row))
		for row in cursor.fetchall()
	]

def buat_sesi_konsultasi(request):
	cursor = connection.cursor()
	cursor.execute('set search_path to medikago;')

	if request.session.get('role') == "ADMIN":
		if request.method == 'POST':
			form = BuatSesiKonsultasi(request.POST)

			if form.is_valid():
				cursor.execute('select id_konsultasi from sesi_konsultasi order by id_konsultasi::int desc limit 1;')
				temp = dictfetchall(cursor)
				id_konsultasi = str(int(temp[0]['id_konsultasi']) + 1)
				biaya = 0
				status = "Booked"
				nomor_rekam_medis_pasien = form.cleaned_data['Nomor_Rekam_Medis_Pasien']
				tanggal = form.cleaned_data['Tanggal']
				id_transaksi = form.cleaned_data['ID_Transaksi']
				cursor.execute("insert into sesi_konsultasi values (" + id_konsultasi + ",'" + 
				str(nomor_rekam_medis_pasien) + "','" + str(tanggal) + "','" + str(biaya) + "','" + str(status) + "','" + str(id_transaksi) + "');")
				return redirect('/sesi-konsultasi/')
			else:
				error = "Data yang anda masukkan tidak valid."
				response = {
					'form' : form,
					'error' : error
				}
				return render(request, 'sesi_konsultasi/konsultasi.html', response)
		else:
			form = BuatSesiKonsultasi()
			response = {'form' : form}
			return render(request, 'sesi_konsultasi/konsultasi.html', response)
	else:
		html = "<html><body>You are not authorized to view this page.</body></html>"
		return HttpResponse(html)

def sesi_konsultasi(request):
	cursor = connection.cursor()
	cursor.execute('set search_path to medikago;')
	cursor.execute('select * from sesi_konsultasi;')
	rows = dictfetchall(cursor)

	if request.session.get('role') == "ADMIN":
		role = "admin"
	else:
		role = "others"


	return render(request, 'sesi_konsultasi/daftar-sesi-konsultasi.html', {'rows':rows, 'role':role})

def update_sesi_konsultasi(request, slug):
	cursor = connection.cursor()
	cursor.execute('set search_path to medikago;')

	if request.session.get('role') == "ADMIN":
		if request.method == 'POST':
			form = UpdateSesiKonsultasi(request.POST)
			if form.is_valid():
				id_konsultasi = form.cleaned_data.get('ID_Konsultasi')
				tanggal = form.cleaned_data.get('Tanggal')
				status = form.cleaned_data.get('Status')
				cursor.execute("update sesi_konsultasi set tanggal='" + str(tanggal) + "'," + "status='" + status + "' where id_konsultasi=" + id_konsultasi + "::varchar;")
				return redirect('/sesi-konsultasi/')
			else:
				response = {'form' : form}
				return render(request, 'sesi_konsultasi/update-sesi-konsultasi.html', response)
		else:
			cursor.execute('select no_rekam_medis_pasien, biaya, id_transaksi from sesi_konsultasi where id_konsultasi =' + slug + '::varchar;')
			temp = dictfetchall(cursor)
			nomor_rekam_medis_pasien = temp[0]['no_rekam_medis_pasien']
			biaya = temp[0]['biaya']
			id_transaksi = temp[0]['id_transaksi']
			form = UpdateSesiKonsultasi(initial = {
					'ID_Konsultasi' : slug,
					'Nomor_Rekam_Medis_Pasien' : nomor_rekam_medis_pasien,
					'Biaya' : biaya,
					'ID_Transaksi' : id_transaksi
				})
			response = {'form' : form}
			return render(request, 'sesi_konsultasi/update-sesi-konsultasi.html', response)
	else:
		html = "<html><body>You are not authorized to view this page.</body></html>"
		return HttpResponse(html)

def delete_sesi_konsultasi(request, slug):
	cursor = connection.cursor()
	cursor.execute('set search_path to medikago;')

	if request.session.get('role') == "ADMIN":
		cursor.execute('delete from sesi_konsultasi where id_konsultasi=' + slug + '::varchar;')
		return redirect("/sesi-konsultasi/")
	else:
		html = "<html><body>You are not authorized to view this page.</body></html>"
		return HttpResponse(html)
