from django import forms
from django.db import connection

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

class BuatTransaksiForm(forms.Form):
    no_rekam_medis = forms.ChoiceField(required = True, widget=forms.Select())
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        cursor = connection.cursor()
        cursor.execute('set search_path to medikago;')
        cursor.execute("SELECT no_rekam_medis FROM Pasien;")
        raw_norek = dictfetchall(cursor)
        no_rekam_medis_list = [tuple([row["no_rekam_medis"], row["no_rekam_medis"]]) for row in raw_norek]
        # print(no_rekam_medis_list)
        self.fields['no_rekam_medis'].choices = no_rekam_medis_list

class UpdateTransaksiForm(forms.Form):
    id_transaksi = forms.CharField(label = 'ID Transaksi', widget=forms.TextInput(attrs={
        'readonly' : True,
        'class' : 'read-only',
    }))
    tanggal = forms.DateField(label = 'Tanggal')
    status = forms.CharField(label = 'Status', widget=forms.TextInput())
    total_biaya = forms.CharField(label = 'Total Biaya', widget=forms.TextInput(attrs={
        'readonly' : True,
        'class' : 'read-only',
    }))
    waktu_pembayaran = forms.CharField(label = 'Waktu Pembayaran')
    no_rekam_medis_pasien = forms.CharField(label = 'Nomor Rekam Medis Pasien', 
        widget=forms.TextInput(attrs={'readonly' : True, 'class' : 'read-only',}))
    