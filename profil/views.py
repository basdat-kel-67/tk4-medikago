from django.shortcuts import render, redirect
from django.db import connection

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def profil_view(request, slug):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "DOKTER":
        role = "dokter"
    elif request.session.get('role') == "ADMIN":
        role = "admin"
    elif request.session.get('role') == "PASIEN":
        role = "pasien"

    cursor.execute("select * from pengguna where username='" + str(slug) + "';")
    users = dictfetchall(cursor)
    return render(request, 'profil/profil.html', {'users':users, 'role':role})