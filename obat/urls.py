from django.conf.urls import url
from django.urls import path
from . import views
from .views import addobat_view, daftarobat_view, updateobat_view, delete_obat_view

appname = 'obat'

urlpatterns = [
    path('create-obat/', addobat_view, name='create obat'),
    path('daftar-obat/', daftarobat_view, name='daftar obat'),
    url(r'^update-obat/(?P<slug>[\w-]+)/$', updateobat_view, name='update'),
    url(r'^delete-obat/(?P<slug>[\w-]+)/$', delete_obat_view, name='delete obat')
]