from django.urls import path
from django.conf.urls import url
from .views import buat_rs_cabang, rs_cabang, update_rs_cabang, daftar_dokter_rscabang, update_dokter_rscabang, dokter_rscabang, delete_rs_cabang, delete_dokter_rscabang

app_name = 'rs_cabang'

urlpatterns = [
	path('buat-rs-cabang/', buat_rs_cabang, name='buat_rs_cabang'),
	path('rs-cabang/', rs_cabang, name='daftar_rs_cabang'),
	url(r'^update-rs-cabang/(?P<slug>[\w-]+)/$', update_rs_cabang, name='update rs'),
    url(r'^delete-rs-cabang/(?P<slug>[\w-]+)/$', delete_rs_cabang, name='delete rs'),
    path('rscabang/daftarkan-dokter/', daftar_dokter_rscabang, name='daftar_dokter_rscabang'),
    path('rscabang/update-dokter/<id>/', update_dokter_rscabang, name='update_dokter_rscabang'),
    path('rscabang/delete-dokter/<id>/', delete_dokter_rscabang, name='delete_dokter_rscabang'),
    path('rscabang/dokter/', dokter_rscabang, name='dokter_rscabang'),
]
