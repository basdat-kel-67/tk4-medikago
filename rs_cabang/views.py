from django.shortcuts import render, redirect
from .forms import BuatRSCabang, UpdateRSCabang, DaftarkanDokter, UpdateDokter
from django.http import HttpResponse
from django.db import connection

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def buat_rs_cabang(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "ADMIN":
        if request.method == 'POST':
            form = BuatRSCabang(request.POST)
            if form.is_valid():
                kode_rs = form.cleaned_data.get('kode_rs')
                nama = form.cleaned_data.get('nama')
                tanggal_pendirian = form.cleaned_data.get('tanggal_pendirian')
                jalan = form.cleaned_data.get('jalan')
                kota = form.cleaned_data.get('kota')
                nomor = form.cleaned_data.get('nomor')
                try:
                    cursor.execute(f"insert into rs_cabang(kode_rs, nama, tanggal_pendirian, jalan, kota, nomor) values('{kode_rs}', '{nama}','{tanggal_pendirian}','{jalan}','{kota}','{nomor}');")
                    return redirect("/rs-cabang/")
                except:
                    error = "RS Cabang dengan kode " + kode_rs + " sudah ada"
                    response = {'form' : form, 'error' : error}
                    return render(request, 'create-rs-cabang.html', response)
            else:
                error = "Data yang anda masukkan tidak valid"
                response = {'form' : form, 'error' : error}
                return render(request, 'create-rs-cabang.html', response)
        else:
            form = BuatRSCabang()
            response = {'form' : form}
            return render(request, 'create-rs-cabang.html', response)
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)

def rs_cabang(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    cursor.execute("select * from rs_cabang")
    rows = dictfetchall(cursor)
    return render(request, 'daftar-rs-cabang.html', {'rows':rows})

def delete_rs_cabang(request, slug):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "ADMIN":
        cursor.execute("delete from rs_cabang where kode_rs='" + str(slug) + "';")
        return redirect("/rs-cabang/")
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)

def update_rs_cabang(request, slug):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "ADMIN":
        if request.method == 'POST':
            form = UpdateRSCabang(request.POST, initial = {
                'kode_rs' : slug
            })
            if form.is_valid():
                nama = form.cleaned_data.get('nama')
                tanggal_pendirian = form.cleaned_data.get('tanggal_pendirian')
                jalan = form.cleaned_data.get('jalan')
                kota = form.cleaned_data.get('kota')
                nomor = form.cleaned_data.get('nomor')
                cursor.execute("update rs_cabang set nama='" + str(nama) + "', tanggal_pendirian='" + str(tanggal_pendirian) + "', jalan='" + str(jalan) + "', kota='" + str(kota) + "', nomor=" + str(nomor) + " where kode_rs='" + str(slug) + "';")
                return redirect("/rs-cabang/")
            else:
                context = {
                    'form' : form
                }
                return render(request, 'update-rs-cabang.html', context)
        else:
            form = UpdateRSCabang(initial = {
                'kode_rs' : slug
            })
            context = {
                'form' : form
            }
            return render(request, 'update-rs-cabang.html', context)
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)

def daftar_dokter_rscabang(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    if request.method == 'POST':
        form = DaftarkanDokter(request.POST)
        if form.is_valid():
            id_dokter = form.cleaned_data.get('id_dokter')
            kode_rs = form.cleaned_data.get('kode_rs')
            cursor.execute("insert into dokter_rs_cabang(id_dokter, kode_rs) values('" + str(id_dokter) + "', '" + str(kode_rs) + "');")
            return redirect("/rscabang/dokter/")
        else:
            context = {
                'form' : form
            }
            return render(request, 'daftar_dokter_rscabang.html', context)
    else:
        form = DaftarkanDokter()
        context = {
            'form' : form
        }
        return render(request, 'daftar_dokter_rscabang.html', context)

def dokter_rscabang(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    cursor.execute("select distinct kode_rs from dokter_rs_cabang")
    rs = dictfetchall(cursor)
    i = 0
    for kode_rs in rs:
        kode_rs['no'] = i+1
        i += 1
        cursor.execute("select id_dokter from dokter_rs_cabang where kode_rs = '" + str(kode_rs.get('kode_rs')) + "';")
        id_dokter = dictfetchall(cursor)
        kode_rs['dokter'] = id_dokter
    if request.session.get('role') == "ADMIN":
        role = "admin"
    else:
        role = "others"
    return render(request, 'dokter_rscabang.html', {'rows':rs, 'role':role})

def update_dokter_rscabang(request, id):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    if request.session.get('role') == "ADMIN":
        if request.method == 'POST':
            form = UpdateDokter(request.POST)
            if form.is_valid():
                kode_rs = form.cleaned_data.get('kode_rs')
                cursor.execute("update dokter_rs_cabang set kode_rs = '" + str(kode_rs) + "' where id_dokter = '" + str(id) + "';")
                return redirect('/rscabang/dokter/')
            else:
                context = {
                    'form' : form
                }
                return render(request, 'update_dokter_rscabang.html', context)
        else:
            cursor.execute("select * from dokter_rs_cabang where id_dokter = '" + str(id) + "';")
            kode = dictfetchall(cursor)
            form = UpdateDokter(initial = {
                'id_dokter' : str(id),
                'kode_rs' : kode[0].get('kode_rs')
            })
            context = {
                'form' : form
            }
            return render(request, 'update_dokter_rscabang.html', context)
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)

def delete_dokter_rscabang(request, id):
    if request.session.get('role') == "ADMIN":
        cursor = connection.cursor()
        cursor.execute('set search_path to medikago')
        cursor.execute("delete from dokter_rs_cabang where id_dokter ='" + str(id) + "';")
        return redirect("/rscabang/dokter/")
    else:
        return redirect("/rscabang/dokter/")
