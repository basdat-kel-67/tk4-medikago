from django import forms
from django.db import connection

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

class UpdateLayananPoliklinik(forms.Form):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    cursor.execute('select nama, kode_rs from rs_cabang;')
    dict_nama = dictfetchall(cursor)
    RS_CABANG = [tuple([row["kode_rs"], row["nama"]]) for row in dict_nama]
    id_poliklinik = forms.CharField(label = 'ID Poliklinik', disabled = True)
    nama_layanan = forms.CharField(label = 'Nama Layanan', max_length = 50, required = True)
    deskripsi = forms.CharField(label = 'Deskripsi', required = False)
    rs_cabang = forms.CharField(label = 'RS Cabang Penyedia Layanan', widget = forms.Select(choices = RS_CABANG), required = True)

class UpdateJadwalLayananPoliklinik(forms.Form):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    cursor.execute('select id_dokter from dokter;')
    dict_id = dictfetchall(cursor)
    ID_DOKTER = [tuple([row["id_dokter"], row["id_dokter"]]) for row in dict_id]
    id_jadwal = forms.CharField(label = 'ID Jadwal Layanan Poliklinik', disabled = True)
    hari = forms.CharField(label = 'Hari', max_length = 50, required = True)
    waktu_mulai = forms.TimeField(label = 'Waktu Mulai', required = True, widget = forms.TimeInput(attrs={
        'type' : 'time'
    }))
    waktu_selesai = forms.TimeField(label = 'Waktu Selesai', required = True, widget = forms.TimeInput(attrs={
        'type' : 'time'
    }))
    kapasitas = forms.IntegerField(label = 'Kapasitas', required = True)
    id_dokter = forms.CharField(label = 'ID Dokter', widget = forms.Select(choices = ID_DOKTER), required = True)
    id_poliklinik = forms.CharField(label = 'ID Poliklinik', disabled = True)