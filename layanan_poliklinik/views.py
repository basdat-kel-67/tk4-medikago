from django.shortcuts import render, redirect
from .forms import UpdateLayananPoliklinik, UpdateJadwalLayananPoliklinik
from django.db import connection
from django.http import HttpResponse

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def create_layanan_poliklinik(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    cursor.execute("select kode_rs_cabang from layanan_poliklinik;")
    kode_rs_rows = dictfetchall(cursor)
    KODE_RS = [kode_rs_cabang["kode_rs_cabang"] for kode_rs_cabang in kode_rs_rows]
    cursor.execute('select id_dokter from dokter;')
    dict_id = dictfetchall(cursor)
    ID_DOKTER = [row["id_dokter"] for row in dict_id]
    if request.session.get('role') == "ADMIN":
        if request.method == 'POST':
            cursor.execute('select id_poliklinik from layanan_poliklinik order by id_poliklinik desc limit 1')
            id_array = dictfetchall(cursor)
            id_poli = str(int(id_array[0]['id_poliklinik']) + 1)
            nama = request.POST['nama']
            deskripsi = request.POST['deskripsi']
            kode_rs = request.POST['kode-rs']
            cursor.execute("insert into layanan_poliklinik(id_poliklinik, kode_rs_cabang, nama, deskripsi) values('" + id_poli + "', '" + str(kode_rs) + "', '" + str(nama) + "', '" + str(deskripsi) + "');")
            list_jadwal = []
            number = 0
            while True:
                try:
                    dict_jadwal = {}
                    tag = 'hari-' + str(number)
                    hari = str(request.POST[tag])
                    dict_jadwal['hari'] = hari
                    tag = 'waktu-mulai-' + str(number)
                    waktu_mulai = str(request.POST[tag])
                    dict_jadwal['waktu-mulai'] = waktu_mulai
                    tag = 'waktu-selesai-' + str(number)
                    waktu_selesai = str(request.POST[tag])
                    dict_jadwal['waktu-selesai'] = waktu_selesai
                    tag = 'kapasitas-' + str(number)
                    kapasitas = str(request.POST[tag])
                    dict_jadwal['kapasitas'] = kapasitas
                    tag = 'id-dokter-' + str(number)
                    id_dokter = str(request.POST[tag])
                    dict_jadwal['id-dokter'] = id_dokter
                    list_jadwal.append(dict_jadwal)
                    number += 1
                except:
                    break
            for jadwal in list_jadwal:
                cursor.execute('select id_jadwal_poliklinik from jadwal_layanan_poliklinik order by id_jadwal_poliklinik desc limit 1')
                id_array = dictfetchall(cursor)
                id_jadwal = int(id_array[0]['id_jadwal_poliklinik']) + 1
                cursor.execute("insert into jadwal_layanan_poliklinik(id_jadwal_poliklinik, waktu_mulai, waktu_selesai, hari, kapasitas, id_dokter, id_poliklinik) values('" + str(id_jadwal) + "', '" + str(jadwal['waktu-mulai']) + "', '" + str(jadwal['waktu-selesai']) + "', '" + str(jadwal['hari']) + "', '" + str(jadwal['kapasitas']) + "', '" + str(jadwal['id-dokter']) + "', '" + str(id_poli) + "');")
            return redirect("/layanan-poliklinik/")
        else:
            context = {
                'kode_rs' : KODE_RS,
                'id_dokter' : ID_DOKTER
            }
            return render(request, 'create_layanan_poliklinik.html', context)
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)

def layanan_poliklinik(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    cursor.execute("select * from layanan_poliklinik")
    rows = dictfetchall(cursor)
    if request.session.get('role') == "ADMIN":
        role = "admin"
    else:
        role = "others"
    return render(request, 'layanan_poliklinik.html', {'rows':rows, 'role':role})

def update_layanan_poliklinik(request, id):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    if request.method == 'POST':
        form = UpdateLayananPoliklinik(request.POST, initial = {
            'id_poliklinik' : str(id),
        })
        if form.is_valid():
            nama_layanan = form.cleaned_data.get('nama_layanan')
            deskripsi = form.cleaned_data.get('deskripsi')
            rs_cabang = form.cleaned_data.get('rs_cabang')
            cursor.execute("update layanan_poliklinik set nama = '" + nama_layanan + "', deskripsi = '" + deskripsi + "', rs_cabang = '" + int(rs_cabang) + "' where id_poliklinik = '" + str(id) + "';")
            return redirect('/layanan-poliklinik/')
        else:
            context = {
                'form' : form
            }
            return render(request, 'update_layanan_poliklinik.html', context)
    else:
        cursor.execute("select * from layanan_poliklinik where id_poliklinik = '" + str(id) + "';")
        poli = dictfetchall(cursor)
        cursor.execute("select nama from rs_cabang where kode_rs = '" + str(poli[0].get('kode_rs_cabang')) + "';")
        rs = dictfetchall(cursor)
        form = UpdateLayananPoliklinik(initial = {
            'id_poliklinik' : str(id),
            'nama_layanan' : poli[0].get('nama'),
            'deskripsi' : poli[0].get('deskripsi'),
            'rs_cabang' : rs[0].get('nama')
        })
        context = {
            'form' : form
        }
        return render(request, 'update_layanan_poliklinik.html', context)

def delete_layanan_poliklinik(request, id):
    if request.session.get('role') == "ADMIN":
        cursor = connection.cursor()
        cursor.execute('set search_path to medikago')
        cursor.execute("delete from jadwal_layanan_poliklinik where id_poliklinik ='" + str(id) + "';")
        cursor.execute("delete from tindakan_poli where id_poliklinik ='" + str(id) + "';")
        cursor.execute("delete from layanan_poliklinik where id_poliklinik ='" + str(id) + "';")
        return redirect("/layanan-poliklinik/")
    else:
        return redirect("/layanan-poliklinik/")

def jadwal_layanan_poliklinik(request, id):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    cursor.execute("select * from jadwal_layanan_poliklinik where id_poliklinik = '" + str(id) + "'")
    rows = dictfetchall(cursor)
    if request.session.get('role') == "ADMIN":
        role = "admin"
    else:
        role = "others"
    return render(request, 'jadwal_layanan_poliklinik.html', {'rows':rows, 'role':role, 'id_poliklinik':id})

def create_jadwal_layanan_poliklinik(request, id):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    if request.session.get('role') == "ADMIN":
        cursor.execute('select id_jadwal_poliklinik from jadwal_layanan_poliklinik order by id_jadwal_poliklinik desc limit 1')
        id_array = dictfetchall(cursor)
        id_jadwal = int(id_array[0]['id_jadwal_poliklinik']) + 1
        if request.method == 'POST':
            form = UpdateJadwalLayananPoliklinik(request.POST, initial = {
                'id_jadwal' : str(id_jadwal),
                'id_poliklinik' : str(id)
            })
            if form.is_valid():
                hari = form.cleaned_data.get('hari')
                waktu_mulai = form.cleaned_data.get('waktu_mulai')
                waktu_selesai = form.cleaned_data.get('waktu_selesai')
                kapasitas = form.cleaned_data.get('kapasitas')
                id_dokter = form.cleaned_data.get('id_dokter')
                cursor.execute("insert into jadwal_layanan_poliklinik(id_jadwal_poliklinik, waktu_mulai, waktu_selesai, hari, kapasitas, id_dokter, id_poliklinik) values('" + str(id_jadwal) + "', '" + str(waktu_mulai) + "', '" + str(waktu_selesai) + "', '" + str(hari) + "', '" + str(kapasitas) + "', '" + str(id_dokter) + "', '" + str(id) + "');")
            return redirect('/jadwal-layanan-poliklinik/' + str(id) + '/')
        else:
            cursor.execute("select * from jadwal_layanan_poliklinik where id_jadwal_poliklinik = '" + str(id_jadwal) + "';")
            jadwal = dictfetchall(cursor)
            form = UpdateJadwalLayananPoliklinik(initial = {
                'id_jadwal' : str(id_jadwal),
                'id_poliklinik' : str(id)
            })
            context = {
                'form' : form
            }
            return render(request, 'create_jadwal_layanan_poliklinik.html', context)
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)

def update_jadwal_layanan_poliklinik(request, id_poli, id_jadwal):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    if request.session.get('role') == "ADMIN":
        if request.method == 'POST':
            form = UpdateJadwalLayananPoliklinik(request.POST, initial = {
                'id_jadwal' : str(id_jadwal),
                'id_poliklinik' : str(id_poli)
            })
            if form.is_valid():
                hari = form.cleaned_data.get('hari')
                waktu_mulai = form.cleaned_data.get('waktu_mulai')
                waktu_selesai = form.cleaned_data.get('waktu_selesai')
                kapasitas = form.cleaned_data.get('kapasitas')
                id_dokter = form.cleaned_data.get('id_dokter')
                cursor.execute("update jadwal_layanan_poliklinik set hari = '" + hari + "', waktu_mulai = '" + str(waktu_mulai) + "', waktu_selesai = '" + str(waktu_selesai) + "', kapasitas = '" + str(kapasitas) + "', id_dokter = '" + str(id_dokter) + "' where id_jadwal_poliklinik = '" + str(id_jadwal) + "';")
            return redirect('/jadwal-layanan-poliklinik/' + str(id_poli) + '/')
        else:
            cursor.execute("select * from jadwal_layanan_poliklinik where id_jadwal_poliklinik = '" + str(id_jadwal) + "';")
            jadwal = dictfetchall(cursor)
            form = UpdateJadwalLayananPoliklinik(initial = {
                'id_jadwal' : str(id_jadwal),
                'hari' : jadwal[0].get('hari'),
                'waktu_mulai' : jadwal[0].get('waktu_mulai'),
                'waktu_selesai': jadwal[0].get('waktu_selesai'),
                'kapasitas' : jadwal[0].get('kapasitas'),
                'id_dokter' : jadwal[0].get('id_dokter'),
                'id_poliklinik' : str(id_poli)
            })
            context = {
                'form' : form
            }
            return render(request, 'update_jadwal_layanan_poliklinik.html', context)
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)

def delete_jadwal_layanan_poliklinik(request, id_poli, id_jadwal):
    if request.session.get('role') == "ADMIN":
        cursor = connection.cursor()
        cursor.execute('set search_path to medikago')
        cursor.execute("delete from jadwal_layanan_poliklinik where id_jadwal_poliklinik ='" + str(id_jadwal) + "';")
        return redirect("/jadwal-layanan-poliklinik/" + str(id_poli) + "/")
    else:
        return redirect("/jadwal-layanan-poliklinik/" + str(id_poli) + "/")