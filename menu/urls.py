from django.urls import path
from . import views
from .views import *

appname = 'menu'

urlpatterns = [
    path('menu-admin/', menu_admin_view, name='menu admin'),
    path('menu-pasien/', menu_pasien_view, name='menu pasien'),
    path('menu-dokter/', menu_dokter_view, name='menu dokter'),
]